#-------------------------------------------------
#
# Project created by QtCreator 2018-12-20T21:27:13
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = MineSweeper
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++17

SOURCES += \
        main.cpp \
        mainwindow.cpp \
    gameboard.cpp \
    highScore.cpp \
    highScoreDialog.cpp \
    highScoreModel.cpp \
    main.cpp \
    mainwindow.cpp \
    mineCounter.cpp \
    minetimer.cpp \
    tile.cpp \
    gameboard.cpp \
    highScore.cpp \
    highScoreDialog.cpp \
    highScoreModel.cpp \
    main.cpp \
    mainwindow.cpp \
    mineCounter.cpp \
    minetimer.cpp \
    tile.cpp \
    gameboard.cpp \
    highScore.cpp \
    highScoreDialog.cpp \
    highScoreModel.cpp \
    main.cpp \
    mainwindow.cpp \
    mineCounter.cpp \
    minetimer.cpp \
    tile.cpp

HEADERS += \
        mainwindow.h \
    gameboard.h \
    highScore.h \
    highScoreDialog.h \
    highScoreModel.h \
    mainwindow.h \
    mineCounter.h \
    minetimer.h \
    tile.h \
    gameboard.h \
    highScore.h \
    highScoreDialog.h \
    highScoreModel.h \
    mainwindow.h \
    mineCounter.h \
    minetimer.h \
    tile.h \
    gameboard.h \
    highScore.h \
    highScoreDialog.h \
    highScoreModel.h \
    mainwindow.h \
    mineCounter.h \
    minetimer.h \
    tile.h

FORMS += \
        mainwindow.ui \
    mainwindow.ui \
    mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

SUBDIRS += \
    MineSweeper.pro \
    MineSweeper.pro

DISTFILES += \
    resources/images/explosion.png \
    resources/images/flag.png \
    resources/images/GameButton.png \
    resources/images/injured_emoji.png \
    resources/images/mine.png \
    resources/images/screenshot.png \
    resources/images/screenshot_lose.png \
    resources/images/screenshot_win.png \
    resources/images/smile_emoji.png \
    resources/images/sunglasses_emoji.png \
    resources/images/tada.png \
    resources/images/transparent.png \
    resources/images/wow_emoji.png \
    resources/images/wrong.png \
    resources/icons/logo.ico \
    resources/stylesheets/tilesheet.qss \
    resources/resources.rc \
    resources/images/explosion.png \
    resources/images/flag.png \
    resources/images/GameButton.png \
    resources/images/injured_emoji.png \
    resources/images/mine.png \
    resources/images/screenshot.png \
    resources/images/screenshot_lose.png \
    resources/images/screenshot_win.png \
    resources/images/smile_emoji.png \
    resources/images/sunglasses_emoji.png \
    resources/images/tada.png \
    resources/images/transparent.png \
    resources/images/wow_emoji.png \
    resources/images/wrong.png \
    resources/icons/logo.ico \
    resources/stylesheets/tilesheet.qss \
    resources/resources.rc

RESOURCES += \
    resources/resources.qrc \
    resources/resources.qrc \
    resources/resourses.qrc \
    resourses.qrc
