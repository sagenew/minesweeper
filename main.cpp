#include <QApplication>
#include <QStyleFactory>

#include "mainwindow.h"
#include "highScore.h"
#include "highScoreModel.h"

int main(int argc, char* argv[])
{
	qRegisterMetaTypeStreamOperators<HighScore>("HighScore");
	qRegisterMetaTypeStreamOperators<HighScoreModel>("HighScoreModel");

	QApplication app(argc, argv);
	QCoreApplication::setApplicationName("minesweeper");

	MainWindow w;
	w.show();

	return app.exec();
}
